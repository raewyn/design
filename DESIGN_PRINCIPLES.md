# Design principles

### Vertical rhythm
* Components should meet grid requirements
* Vertical rhythm should be met 1x, 2x, 3x
* Avoid hardcoding arbitrary spacing/padding values

### Mobile friendly
* Components and layouts should be responsive
* Scale text to suit screen size when required

### Conform to standards
* HTML5 / semantic markup compliant
* Should pass accessibility tests (compliant to WCAG, Aria)

### Clear documentation
* Atomic design principles (atoms, molecules, organisms?, templates?)
* Design elements must be documented

### Design in practice
* Vanilla should have an un-opinionated visual style (no frills, just a solid starting point to enhance visually)
* Font hierarchy should adhere to our modular scale
* When choosing fonts match typography in a harmonious manner (size, x-height, visual weight, style contrast)

### User experience
* Form elements should be clearly labelled
* User actions should have clear on-screen feedback
* Feedback should have a clear relation to the element it is associated with
* Labelling should be meaningful and use plain language (eg. no internal jargon for authors)
* Actions should be understandable by both look and words (Buttons can have icons but should include text. Icons should have clear semantic meaning. Small screens may be the exception for icon only)
* Tasks should have a logical flow that meets the expectations of the person using the system

### Development
* Do not misuse element attributes (e.g. use input placeholder instead of a label)
* Use dynamic units instead of pixels

### Good behaviour
* User test designs
* Accessibility test using the following tools (to do...)
* Participate in community/sharing of work
