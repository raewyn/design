# Basics of theming

## Background Reading
We rely heavily on concepts of atoms and molecules as a design theory. Please see http://bradfrost.com/blog/post/atomic-web-design/

The components we are most concerned about are atoms, molecules, and pages.

## Three ways of theming

### Level 1: Changing variables

Each atom is set to look after specific variables that let you change the look and feel of all the applications. You'll be able to set the colors, the fonts, the border, the spacing units and other. 
Adjusting the variables of the vanilla to control the look and feel of the component.

### Level 2: Add css properties to your component

CSS is moving fast and we can not plan all the new features that will come soon. For example, you may want to use css-grid for your layout, or you may want to stick with flexbox because you want to support IE9 and friends.
We believe that the theme engin should not force you to use any properties that would fit your needs better. Therefore, you're able to add css properties from your theme. For example, you may want to have your figure grayscale unless you're hovering it. 

### Level 3: Create  a specific component

If the functionnalities you want are outside the scope of the components already in the library, you can create a new component, and share it with the community. To do so, your component needs to comply to the design principles that drive all pubsweet components (see the The [*starter-kit* aka *vanilla*  aka *will-find-a-better-name* theme & its principles](#)). 

## Developing a theme

One of the principles behind the theming engine is that it more than *Don't repeat yourself*,  it's also about how you *don't repeat others*. If somebody has made a component that you wanna use, you should't need to modify the component itself so it can work with your idea of UX and UI.

Therefore, the design workgroup agreed on a way to share components with as little code as possible, easy to maintain, update and extend using the theme engine.

To design is to create relationship between elements on a surface: paper or screen (or walls, or *add your own thing here*). Those relationships are based on similarities and differences, proximity and remoteness, contrasts, speeds, etc. Those relationships affect fonts, sizes, colors, spacing, shapes, behaviors, etc. 

The theory of *Gestalt*, on which a lot of design rules are based today, explains that the whole is not the same than the sum of all the elements. In terms of design, it means that a simple change on the smallest component is able to change the look and feel of the whole experience. Therefore, when working on small bit of the whole application, you need to be sure that you dont change the relationship between elements by adding css properties. How do you limit the risk of unwanted relationship between components?

## Consistency.

Inside your app, you want to use the same colors, fonts, animations. Your buttons should all behave the same, the link should all have the same look. Tooltip should use the same font accross the application, things should work the way the user want those to work (unless you have good reason to do otherwise). It will add more meaning and values to each component: easier to maintain, easier to understand and you can deviate from it to add another meaning.

This can be tricky when there are a bunch of different teams working on the same project, with their own habits, likes and tastes. Each team is designing a small piece of the app, that will be part of the bigger puzzle. How can you ensure that each component will work with the rest? How do you help dev and designers to stay consistent? 

You need a design system.

# A system for the design

A design system is a living example of all the relationships that govern your application. On a more pratical level, it's a way to show all your UI component, how they look, how they behave, what feedback they gave you. It also show how each component is used inside a specific environment, how responsiveness is handled for each element. And also, it should be able to show complete page template to see each component in its environment.

To develop its own design system, Pubsweet is using [Styleguidist](https://github.com/styleguidist/react-styleguidist) to display the components of the UI, sorterd by atomic level and alphabetically.


### Atomic what?

Atomic level.

When you're starting to build small pieces of software that should be reused in other bit of software, you end up by diving your component into the smaller pieces possible to reuse.

While we were already working that way at Coko, we found Brad Frost *Atomic design* concepts. There is a complete available book on the subject by Brad himself that you can find [here](http://atomicdesign.bradfrost.com/table-of-contents/) if you want to get deeper into it (spoiler: you should). 

In short, it's a way to divide your UI elements in small bit and to look at it as a molecular level: 

* Templates are made of one or many organisms;
* Organisms are made of one or many molecules;
* Molecules are made of one or many atoms.
* Atom is the smallest bit of UI.

For example, a login form (the organism) will be created using a molecule (made of two different atoms `<input>` & `<label>`) and an atom of `<button>`. 

The good thing about that is:
* you don't have to recreate a button every time you need one;
* you don't have to style it again using CSS (but you can still tweak it if needed).

Reusing the same atom in multiple components in the only way to insure consistency across the whole app, specifically when the development is made by different teams, on a atomic level.

### Variables

Atomic design is a good way to ensure consistency when using the same component accross the application. But we also need to be sure that the relationships between elements shoudl stay consistent: colors, fonts and spacing should be shared across the app.

To be sure that the component is reusable in multiple UI/UX environment, we must provide a way for the designer/dev to change values of the visual look and feel of the component. To do so, we provide variables that are shared through all the components.

The issue with variables is that you can easily set your foot in a variables mayhem, where everything is a variable, and each property has one. That's why we came with the smallest list of variables that you can find below.

#### Colors

|variable|comments|
|---|---|
|`colorPrimary`|Indicates a primary call to action
`colorSecondary`|Default color for non-primary actions
`colorFurniture`|Meant to be applied to elements that indicate content division
`colorBorder`|For borders around form elements
`colorBackgroundHue` | Used to create a discrete contrast with the default background color
`colorSuccess`| Used to indicate a successful validation state
`colorError`| Used to indicate an error in validation
`colorText`| Default font color
`colorTextReverse`| Reverse font color
`colorTextPlaceholder`| Used for text field placeholders

####  Typography

|variable|comments
|---|--
|`fontInterface`|Used for user interface elements by default
|`fontHeading`| Used for headings
|`fontReading`| Main reading text
|`fontWriting`| Font used for writing
|`fontSizeBase`|Default font size
|`fontSizeBaseSmall`| Smaller variation of fontSizeBase
|`fontSizeHeading1`| Size for Heading 1
|`fontSizeHeading2`|Size for Heading 2
|`fontSizeHeading3`|Size for Heading 3
|`fontSizeHeading4`|Size for Heading 4
|`fontSizeHeading5`|Size for Heading 5
|`fontSizeHeading6`|Size for Heading 6
|`fontLineHeight`| Default line height

####  Spacing

|variable|comments
|---|---
`gridUnit`|Base interface space measurement used by elements and typography

####  Border

|variable|comments
|---|---
`borderRadius`| Radius value applied to borders throughout the user interface
`borderWidth`|Width value applied to borders
`borderStyle`|Style applied to borders (eg. solid, dashed)

####  Shadow
|variable|comments
|---|---
`dropShadow`| Default shadow that is applied to elements that float (eg. tooltips, modals)

####  Transition
|variable|comments
|---|---
`transitionDuration`|How long transitions should last
`transitionTimingFunction`|Which function should be applied to transitions (eg. `easein`)
`transitionDelay`|How long transitions should be delayed before they begin



All component that need to get a primary color will be ready for the variable.

For example, let's look at a button primary styles: 

```css
button.primary {
  background: var(--colorPrimary);
  color: var(--colorTextReverse);
  margin: 0 0 calc(var(--gridUnit) - var(--border-width)) 0 ;
  border-width: var(--borderWidth);
  border-style: var(--borderStyle);
  border-radius: var(--borderRadius);
  border-color: var(--colorBorder);
}
```

The css for the component is waiting for a set of values for those variables, which are defined in the theme component. Therefore, any company will have its own style for the same component.

*later: what about creating layout using variables, so even the layout would be configurable within the css variables.

Interrestingly, firefox showed me this random quote today, that i think works pretty wel with what we want to do: using a design system to win some time, not to do all the design for us:

> Free thought: Never let a dress code get in the way of fun socks.




## in Practice

### The *starter-kit* aka *vanilla*  aka *will-find-a-better-name* theme & its principles

While we can test component in different theming, by randomly changin the values of the variables for example (which would be time consuming and not bulletproof), we decided on the idea of a vanilla theme. 

The vanilla is the theme that comes with pubsweet right after installing. It's a set of foundations to build a new theme, and a UI/UX testing environment. 

When creating a UI/UX component, the only way to be sure that it can be reused in different pubsweet app, is to see how it fits inside the vanilla theme. Since we're using the same variables accross themes, it is pretty easy to test the component in different themes environment.

When designing a component, the best path is to design it for the *starter-kit* first: the component should not get other css properties than the one set by the list of variables and should work fluently in the vanilla theme. If your component need to get specific css attribute for your own styling, add those in your theme file and not on the component itself. 

For example, let's say that all link in your theme have a blue background, but, for a specific component, they should have a pink one. This is something specific to your design, and not to the *starter-kit*. Your styling will go into your theme package.

This is the only way to ensure that the component will be usable in all the pubsweet universe, and that every one can collaborate on it.

To keep this idea real, we need to agree on a set of principles that we should find on all vanilla component. Let's get through those one at a time.

*Everything is not here yet, but it's a good start*

#### Each component conforms to standards.

All component made for pubsweet must be compliant to semantic HTML5. For example, a dropdown menu must be a `<select>` element and each answer should be set as `<option>`. A text field should be an `<input>`. For a complete list of html elements we can use, you can look at the up-to-date [Mozilla's Developer Netword *web docs*](https://developer.mozilla.org/en-US/docs/Web/HTML/Element).


#### Accessibility first

Accessibility must be an important input in the choices of design and UI/UX made for pubsweet.

Therefore, all components should be designed with accessibility in mind: 
- [Accessible Rich Internet Applications (ARIA) attributes](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA) ] should be set everywhere needed.
- Colors should be chosen with contrast in mind and ratio should pass at least the AA grade for the [WCAG 2.0](https://www.w3.org/TR/UNDERSTANDING-WCAG20/conformance.html) test.

Some readings: https://www.w3.org/standards/webdesign/accessibility
Authoring Tool Accessibility Guidelines (ATAG) from the W3C is also a good read  for what we're doing https://www.w3.org/WAI/intro/atag.php.

The #a11y projcet github page full of tips and tricks https://github.com/a11yproject/a11yproject.com

There is no automated accessibility test today. Therefore we can only rely on the contributors of the projects to check and test each component. We can use that checklist to begin with: https://a11yproject.com/checklist

It would be interresting to have an Accessibility team (the A-team) to check all  component before accepting merge request.

*More to be added here.*



#### On documentation

All components should have a clear documentation, explaining what is it for, how is it used, and some example of how it behaves.

Pubsweet is using [Styleguidist](https://github.com/styleguidist/react-styleguidist) to display the components of the UI, sorterd by atomic level and alphabetically.

Each component showed in styleguidist should: 

- display multiple state of the component (idle, hover, focus, active, etc.);
- display how the component behavior (on mouse over, on click, etc.);
- display if the component can have a specific class/style (primary button, colored radiobutton, etc.)
- display the code needed for the component to be used;
- display the responsiveness of the component. 
- describe the ARIA used in the component
- attribution to the person/team who submit the component

Styleguidist is actually THE living design system, and as such, should keep track of the design discussion.

#### Mobile friendly

All component should be usable on multiple screen size. 
The layout should be responsive. 
The component should have the smallest size possible (all files should be minified, svg should be zipped and classe names should be reduced)
Use dynamic units instead of pixels to ensure that the mobile experience will be easier to build/maintain. If you want to use pixel, be sure that they get transformed to em/rem on the fly.

#### On vertical rhythm

A vertical rhythm is a way to bring consistency in spacing, so each ui component fits into place and none seems awkward (unless you design it that way). 

Each component should be set on that grid.

Here are a bunch of good practice to ease the use of a grid:

- Setting the line-height of your body text as a gridUnit is a good way to keep a coherent relationship between your text and your elements;
- Use a scale for typographic measurements, so your text always fit in `n * gridUnit`;
- You can use `box-sizing: border-box` as a basic way to ease the fitting to grid to an element with a specified height, but `box-sizing` only works when you height is specified which reduce the component usability);
- in html, the height of an element is always the result of this formula: `border-top + padding-top + content + padding bottom + border-bottom`. Be sure that the result of the calculation is always a multiple of your gridUnit. If you're using css variables, you can make some calculation to get the  component right: `padding: calc(var(--gridUnit) / 2 - var(--borderWidth)) calc(var(--gridUnit) / 2)` 
- Never set a margin-top, always use the margin bottom to separate element.



To test the vertical rhythm of your component, here is a snippet that will create a background made of lines, using the variables from the theme. You can set it to your body or to your component.

```css
  --gridUnit: 24px;
  --grid-color: rgba(122,200,122, 0.5);
  --subgrid: calc(var(--gridUnit) / 2 );
  
	background-image: linear-gradient(
 		to bottom, 
		transparent calc(var(--subgrid) - 1px), 
		var(--grid-color) var(--subgrid)
	);
  background-size: 100% var(--subgrid);
  background-position: left 0px top var(--subgrid);
  line-height: var(--fontLineHeight);

```

![grid](images/grid.png)



(the why and how the grid work with real case example)
(the why and how of the typographic scale)
(When choosing fonts match typography in a harmonious manner psize, x-height, visual weight, style contrast])

#### On User experience & development
- Form elements should be clearly labelled
- User actions should have clear on-screen feedback
- Feedback should have a clear relation to the element it is associated with
- Labelling should be meaningful and use plain language (eg. no internal jargon for authors)
- Actions should be understandable by both look and words (Buttons can have icons but should include text. Icons should have clear semantic meaning. Small screens may be the exception for icon only)
- Tasks should have a logical flow that meets the expectations of the person using the system
- The component should be tested by real users and the conclusions of the test should be accessible.

---

- The layout is always handle by the parent component (molecules handle the layout of atoms, organism handle the layout of molecules), so it's easier to manage whole webpage by only looking at the first level of children
- Each molecule should have props for different layout. For example, a label and an input can be one on top of the other, or one next to each other. Adding a prop to decide that should be always possible. 


#### On sharing
How to participate in community, sharing of work. What spaces are provided for this in pubsweet universe?